<?php
// include mysql.php file for $user and $password variables
include 'mysql.php';

function database()
{
   global $user;
   global $password;

    try
    {
        $db = new PDO('mysql:host=localhost;dbname=bdd_annuaire;charset=utf8',$user,$password);
    }
    catch(Exception $e)
    {
        die('Erreur : '. $e->getMessage());
    }
    return $db;
}

function thumbnails() {
    $thumbnails = array();

    $pdo = database();
    $query = $pdo->query('SELECT title, thumbnail FROM movies ORDER BY title');

    while ($row = $query -> fetch()) {
        array_push($thumbnails, $row);
    }

    return $thumbnails;
}

function title()
{
	$movies = array();

    $pdo = database();
    $movieTitle = $pdo -> prepare('SELECT id_movie, title, release_date, director, thumbnail
    FROM movies ORDER BY title ASC');

    $movieTitle -> execute();

    while ($row = $movieTitle -> fetch()){
        array_push($movies, $row);
    }
    return $movies;
}

function DESC_title()
{
    $movies = array();

    $pdo = database();
    $movieTitle = $pdo -> prepare('SELECT id_movie, title, release_date, director, thumbnail
    FROM movies ORDER BY title DESC');

    $movieTitle -> execute();

    while ($row = $movieTitle -> fetch()){
        array_push($movies, $row);
    }
    return $movies;
}

function search()
{
    $search = $_POST['research'];

    $movies = array();

    $pdo = database();

    $sql = "SELECT id_movie, title, release_date, director, thumbnail
        FROM movies WHERE title LIKE :search OR release_date LIKE
        :search OR director LIKE :search ORDER BY title";

    $query = $pdo -> prepare($sql);
    $query -> execute(['search' => "%$search%"]);

    while ($row = $query -> fetch()) {
        array_push($movies, $row);
    }

    return $movies;
}
