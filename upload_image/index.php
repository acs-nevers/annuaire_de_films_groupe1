<?php
$msg = "";
// if upload button is pressed
if (isset($_POST['upload'])) {
   //the path to store the upload image
    $target = "images/".basename($_FILES['image']['name']);

    //connect to the database
    try
    {
        // On se connecte à MySQL
        $bdd = new PDO('mysql:host=localhost;dbname=bdd_annuaire;charset=utf8', 'Apache', 'adrienAVR58');
    }
    catch(Exception $e)
    {
        // En cas d'erreur, on affiche un message et on arrête tout
            die('Erreur : '.$e->getMessage());
    }

    //Get all the submitted data from the form
    $image = $_FILES['image']['name'];
    $text = $_POST['text'];



    $req = $bdd->prepare("INSERT INTO images (image, text) VALUES (:image, :text)");

    $req->execute(array(
        'image' => $image,
        'text' => $text
    ));

    //Now let's move the uploaded image into the folder: images
    if (move_uploaded_file($_FILES['image']['tmp_name'], $target)) {
        $msg = "Image uploaded successfully";
    }
    else {
        $msg = "There was a problem uploading image";
    }
}


?>


<!DOCTYPE html>
<html>
<head>
    <title>Image Upload</title>
    <link rel="stylesheet" type="text/css" href="style.css">

</head>
<body>
    <div id_img="content">
<?php

    try {
        // On se connecte à MySQL
        $bdd = new PDO('mysql:host=localhost;dbname=bdd_annuaire;charset=utf8', 'Apache', 'adrienAVR58');
    }
    catch(Exception $e) {

    die('Erreur : '.$e->getMessage());
    }

    $sql = "SELECT * FROM images";
    $result = $bdd->query($sql);
        while($row = $result->fetch()) {
         echo "<div id='img_div'>";
            echo "<img src='images/".$row['image']."' >";
            echo "<p>".$row['text']."</p>";
         echo "</div>";
        }

?>

    <form method="post" action="index.php" enctype="multipart/form-data">
            <input type="hidden" name="size" value="1000000">
        <div>
            <input type="file" name="image">
        </div>
        <div>
            <textarea name="text" cols="40" rows="4" placeholder="say something about this image..."></textarea>
        </div>
        <div>
            <input type="submit" name="upload" value="Upload Image">
        </div>
    </form>
    </div>
</body>
</html>