<!DOCTYPE html>
<html>

<head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <title>Error 500</title>
    <link rel='stylesheet/less' href='../resources/less/style.less'>
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>
    <script src='../resources/js/less.js' type='text/javascript'></script>
</head>

<body>
    <header id='main-header'>
        <div id='title'>
            <img src='../resources/img/logo.png'>
            <h1>Modaba</h1>
        </div>
    </header>
    <section id='carousel'>
    </section>
    <section id='movies'>
        <ul>
            <li class='movie'>
              <br>
               <h2>Error 500 : Internal Server Error</h2>
               <p>le serveur a rencontré une erreur interne (erreur de script ou erreur passagère).</p>
                <div class='film'>
                </div>
            </li>
        </ul>
    </section>

    <script type='text/javascript' src='../resources/js/document.js'></script>
</body>

</html>